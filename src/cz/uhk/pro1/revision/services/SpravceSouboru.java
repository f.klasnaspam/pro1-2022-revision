package cz.uhk.pro1.revision.services;

import cz.uhk.pro1.revision.model.Zavodnik;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

public class SpravceSouboru {
    
    public void ulozZavodniky(List<Zavodnik> zavodnici, String jmenoSouboru) {
//        zavodnici.sort(new Comparator<Zavodnik>() {
//            @Override
//            public int compare(Zavodnik z1, Zavodnik z2) {
//                return z1.getCas() - z2.getCas();
//            }
//        });
        zavodnici.sort(Comparator.comparing(Zavodnik::getCas));
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(jmenoSouboru))) {
            for (int i = 0; i < zavodnici.size(); i++) {
                bw.write((i+1) + " " + zavodnici.get(i).toString());
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
