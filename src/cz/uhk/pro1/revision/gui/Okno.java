package cz.uhk.pro1.revision.gui;

import cz.uhk.pro1.revision.model.Zavod;
import cz.uhk.pro1.revision.model.Zavodnik;
import cz.uhk.pro1.revision.services.SpravceSouboru;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Okno extends JFrame {

    private Zavod zavod = new Zavod();
    private SpravceSouboru spravceSouboru = new SpravceSouboru();
    private JTextArea taZavodnici = new JTextArea(10, 30);
    private JTextField tfJmeno = new JTextField();
    private JTextField tfPrijmeni = new JTextField();
    private JTextField tfCas = new JTextField();
    private JLabel lblVitez = new JLabel(" ");
    private JTextField tfVyhledat = new JTextField("Příjmení");
    private JLabel lblVyhledat = new JLabel(" ");

    public Okno() {
        setTitle("Evidence závodníků");
        initGui();
        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initGui() {
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        add(new JScrollPane(taZavodnici));
        taZavodnici.setEditable(false);

        JPanel pnlNovyZavodnik = new JPanel();
        pnlNovyZavodnik.setLayout(new GridLayout(2, 4));
        pnlNovyZavodnik.add(new JLabel("Jméno"));
        pnlNovyZavodnik.add(new JLabel("Příjmení"));
        pnlNovyZavodnik.add(new JLabel("Čas"));
        pnlNovyZavodnik.add(new JLabel());
        pnlNovyZavodnik.add(tfJmeno);
        pnlNovyZavodnik.add(tfPrijmeni);
        pnlNovyZavodnik.add(tfCas);
        JButton btnPridat = new JButton("Přidat");
        btnPridat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pridejZavodnika();
            }
        });
        pnlNovyZavodnik.add(btnPridat);
        add(pnlNovyZavodnik);

        JPanel pnlVyhledat = new JPanel();
        pnlVyhledat.add(tfVyhledat);
        JButton btnVyhledat = new JButton("Vyhledat");
        btnVyhledat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                vyhledejZavodnika();
            }
        });
        pnlVyhledat.add(btnVyhledat);
        tfVyhledat.setPreferredSize(btnVyhledat.getPreferredSize());
        add(pnlVyhledat);
        lblVyhledat.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(lblVyhledat);

        JButton btnVitez = new JButton("Vítěz");
        btnVitez.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                zjistiViteze();
            }
        });
        btnVitez.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(btnVitez);
        lblVitez.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(lblVitez);

        JButton btnUlozit = new JButton("Uložit do souboru");
        btnUlozit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ulozZavodniky();
            }
        });
        btnUlozit.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(btnUlozit);
    }

    private void pridejZavodnika() {
        Zavodnik zavodnik = new Zavodnik(tfJmeno.getText(), tfPrijmeni.getText());
        zavodnik.setCas(Integer.parseInt(tfCas.getText()));
        zavod.addZavodnik(zavodnik);
        taZavodnici.append(zavodnik + "\n");
        lblVitez.setText(" ");
        tfJmeno.setText("");
        tfPrijmeni.setText("");
        tfCas.setText("");
    }

    private void zjistiViteze() {
        lblVitez.setText(zavod.getVitez().toString());
    }

    private void vyhledejZavodnika() {
        List<Zavodnik> nalezeni = zavod.getZavodnikByPrijmeni(tfVyhledat.getText());
        if (nalezeni.isEmpty()) {
            lblVyhledat.setText("Nenalezeno");
        } else {
            String vysledekHledani = "";
            for (Zavodnik zavodnik : nalezeni) {
                vysledekHledani += zavodnik + ", ";
            }
            lblVyhledat.setText(vysledekHledani);
        }

    }

    private void ulozZavodniky() {
        spravceSouboru.ulozZavodniky(zavod.getZavodnici(), "zavod.txt");
    }

}
