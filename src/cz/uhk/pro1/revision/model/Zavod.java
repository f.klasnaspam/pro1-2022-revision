package cz.uhk.pro1.revision.model;

import java.util.ArrayList;
import java.util.List;

public class Zavod {

    private List<Zavodnik> zavodnici = new ArrayList<>();

    public List<Zavodnik> getZavodnici() {
        return zavodnici;
    }

    public void addZavodnik(Zavodnik zavodnik) {
        zavodnici.add(zavodnik);
    }

    public List<Zavodnik> getZavodnikByPrijmeni(String prijmeni) {
        List<Zavodnik> zavodniciSPrijmenim = new ArrayList<>();
        for (Zavodnik zavodnik : zavodnici) {
            if (zavodnik.getPrijmeni().equals(prijmeni)) {
                zavodniciSPrijmenim.add(zavodnik);
            }
        }
        return zavodniciSPrijmenim;
    }

    public Zavodnik getVitez() {
        Zavodnik vitez = zavodnici.get(0);
        for (int i = 1; i < zavodnici.size(); i++) {
            if (zavodnici.get(i).getCas() < vitez.getCas()) {
                vitez = zavodnici.get(i);
            }
        }
        return vitez;
    }

}
