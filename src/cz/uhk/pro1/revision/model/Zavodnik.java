package cz.uhk.pro1.revision.model;

public class Zavodnik {

    private String jmeno;
    private String prijmeni;
    private int cas; // soutěžní čas v sekundách; jinak možno reprezentovat například pomocí java.util.Date

    public Zavodnik(String jmeno, String prijmeni) {
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
    }

    public String getJmeno() {
        return jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public int getCas() {
        return cas;
    }

    public void setCas(int cas) {
        this.cas = cas;
    }

    @Override
    public String toString() {
        int h = cas / 3600;
        int m = (cas % 3600) / 60;
        int s = (cas % 3600) % 60;
        return jmeno + " " + prijmeni + " " + h + ":" + m + ":" + s;
    }

}
