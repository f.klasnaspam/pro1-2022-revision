package cz.uhk.pro1.revision.app;

import cz.uhk.pro1.revision.model.Zavod;
import cz.uhk.pro1.revision.model.Zavodnik;

import java.util.List;

public class Aplikace {

    public static void main(String[] args) {

        Zavod zavod = new Zavod();
        Zavodnik z1 = new Zavodnik("Jirka", "Rychlý");
        Zavodnik z2 = new Zavodnik("Honza", "Rychlejší");
        Zavodnik z3 = new Zavodnik("Pepa", "Nejrychlejší");
        zavod.addZavodnik(z1);
        zavod.addZavodnik(z2);
        zavod.addZavodnik(z3);
        z1.setCas(1287);
        z2.setCas(1221);
        z3.setCas(1194);

        List<Zavodnik> vysledkyHledani = zavod.getZavodnikByPrijmeni("Rychlý");
        for (Zavodnik zavodnik : vysledkyHledani) {
            System.out.println(zavodnik);
        }

        Zavodnik vitez = zavod.getVitez();
        System.out.println(vitez);

    }

}
