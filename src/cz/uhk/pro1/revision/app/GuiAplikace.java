package cz.uhk.pro1.revision.app;

import cz.uhk.pro1.revision.gui.Okno;

import javax.swing.*;

public class GuiAplikace {

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Okno().setVisible(true);
            }
        });

    }

}
